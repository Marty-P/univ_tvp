import numpy

class Tree:
	def __init__(self, value, transition = ' ', lock = 0, numPos = 0):
		self.value = value
		self.numPos = numPos
		self.lock = lock
		self.transition = transition
		self.branches = []

	def __str__(self):
		return str(self.value)
	
	def addBranche(self, branche):
		self.branches.append(branche)
	
def printTree(tr, space = 1):
	if tr.lock:
		out ='p '
	else:
		out = ''
	out = out + 't'+str(tr.transition)+' M'+str(tr.numPos)+' '+str(tr.value)
	
	print(('%+'+str(space*13)+'s') % out)
	if len(tr.branches) != 0:
		for i in tr.branches:
			printTree(i,space+1)
	
class NetPetriBW:
	def __init__(self):
#		self.p = int(input('S: '))
#		self.t = int(input('t: '))
#		self.f = numpy.empty((p,t))
#		self.m = numpy.empty((1,p))
		
#		print('\n input matrix f:')
#		for i in range(0,p):
#			for j in range(0,t):
#				f[i][j] = int(input('['+str(i)+']['+str(j)+']: '))
		
#		print('\n input M0:')
#		for i in range(0,p):
#			self.m[0][i] = int(input('['+str(i)+']:'))
			
#		self.depthSearch = int(input('deep: '))
		self.mapSpace = []
		
		self.p = 4
		self.t = 3
		self.f = numpy.array([[-2,1,0],
							[1,-2,0],
							[0,1,-1],
							[0,-1,2]])
							
		self.depthSearch = 3
		self.tree = Tree(numpy.array([2,2,2,2]),' ',0,0)
		self.listPos = [[self.tree.value],[self.tree.numPos]]
		self.tree = self.searchTree_new(Tree(self.tree.value))
		#self.tree = self.searchTree(self.m)
		
		
	def searchTree(self, m, depth = 0):
		branche = Tree(m)
		newM = numpy.empty(self.p)
		if depth < self.depthSearch:
			for i in range(0, self.t):
				newM = m + self.f[:,i]
				if numpy.min(newM) >= 0:
					branche.addBranche(self.searchTree(newM, depth+1))
		
		return branche
		
		
	def searchTree_new(self, tr, depth=0):
		if not(tr.lock):
			if depth < self.depthSearch:
				newM = numpy.empty(self.p)
				for i in range(0, self.t):
					newM = tr.value + self.f[:,i]
					if numpy.min(newM) >= 0:
						flag = 1
						numPos = 0
						lenList = len(self.listPos[0])
						for j in range(0,lenList):
							if numpy.array_equal(self.listPos[0][j], newM):
								flag = 0
								numPos = j
								break
						if flag:
							self.listPos[0].append(newM)
							self.listPos[1].append(self.listPos[1][-1]+1)
							numPos = self.listPos[1][-1]
						tr.addBranche(Tree(newM, i+1, not(flag), numPos))
						
				amountM = len(tr.branches)
				for i in range(0, amountM):
					tr.branches[i] = self.searchTree_new(tr.branches[i], depth+1)
					
		return tr
			
					
				